﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using HomeTask5.Interfaces;

namespace HomeTask5
{
    public class Container
    {
        private static Container _instanse;
        private static IUnityContainer _container;

        private Container(string filePath)
        {
            _container = new UnityContainer();
            _container.RegisterType<ISerializer, CSVSerializer>();
            _container.RegisterType<IFileWriter, FileWriter>();
            _container.RegisterInstance<ISettings>(Settings.GetInstanse(filePath));
        }

        public IUnityContainer GetContainer()
        {
            return _container;
        }

        public static Container GetInstanse(string filePath)
        {
            return _instanse ?? (_instanse = new Container(filePath));
        }
       
    }
}
