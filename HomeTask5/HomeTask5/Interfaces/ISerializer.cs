﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HomeTask5.Interfaces
{
    public interface ISerializer
    {
        string Serialize(User user);
    }
}