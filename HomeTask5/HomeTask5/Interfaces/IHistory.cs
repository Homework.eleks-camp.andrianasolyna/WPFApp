﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace HomeTask5.Interfaces
{
    interface IHistory
    {
        void AddUser(User user);
        void AddMessage(string message);
        string GetHistory();
    }
}
