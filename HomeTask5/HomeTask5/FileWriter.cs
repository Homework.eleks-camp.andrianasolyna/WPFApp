﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomeTask5.Interfaces;

namespace HomeTask5
{
    public class FileWriter: IFileWriter
    {
        public ISettings Settings { get; set; }
        public ISerializer Serializer { get; set; }

        public FileWriter(ISerializer serializer, ISettings settings)
        {
            Serializer = serializer;
            Settings = settings;
        }
        public void Write(User user)
        {
            try
            {
                using (var writer = new StreamWriter(Settings.FilePath, true))
                {
                    writer.WriteLine(Serializer.Serialize(user));
                }
            }
            catch (Exception)
            {
                throw;
            }
           
        }
    }
}
