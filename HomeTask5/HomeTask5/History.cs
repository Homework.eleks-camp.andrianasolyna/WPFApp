﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomeTask5.Interfaces;

namespace HomeTask5
{
    public class History: IHistory
    {
        private List<string> _history;
        private DateTime _time = DateTime.Now;
        private const string AddedUser =" added user ";

        private void CreateHistoryIfNull()
        {
            if (_history == null)
            {
                _history = new List<string>();
            }
        }
        public void AddUser(User user)
        {
            CreateHistoryIfNull();
            var builder = new StringBuilder();
            builder.Append(_time.Hour);
            builder.Append(':');
            builder.Append(_time.Minute.ToString());
            builder.Append(AddedUser);
            builder.Append(user.FirstName);
            builder.Append(' ');
            builder.Append(user.LastName);
            _history.Add(builder.ToString());
        }

        public void AddMessage(string message)
        {
            CreateHistoryIfNull();
            _history.Add(message);
        }
        public string GetHistory()
        {
            var builder = new StringBuilder();
            foreach (var item in _history)
            {
                builder.Append(item);
                builder.Append('\n');
            }
            return builder.ToString();
        }
    }
}
