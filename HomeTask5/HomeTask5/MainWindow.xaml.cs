﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using HomeTask5.Interfaces;
using Microsoft.Practices.Unity;

namespace HomeTask5
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly IHistory _history = new History();
   
        public MainWindow()
        {
            InitializeComponent();
        }

        public void Button_Click(object sender, RoutedEventArgs e)
        {
            var container = Container.GetInstanse(PathTextBox.Text);

            User user = null;
            try
            {
                user = CheckUser();
            }
            catch (ApplicationException)
            {
                _history.AddMessage("First name and last name can't be empty");
            }
            catch
            {
                _history.AddMessage("Incorrect age");
            }

            if (user != null)
            {
                var fileWriter = container.GetContainer().Resolve<IFileWriter>();
                try
                {
                    fileWriter.Write(user);
                    _history.AddUser(user);
                }
                catch
                {
                    _history.AddMessage("Invalid file path");
                }
            }
            ScrollViewer.Content = _history.GetHistory();
            CleanTextBoxes();
        }

        private User CheckUser()
        {
            int age;
            try
            {
                age  = int.Parse(AgeextBox.Text);
            }
            catch (Exception)
            {
                throw;
            }
            var firstName = FirstNameTextBox.Text;
            var lastName = LastNameTextBox.Text;
            
            if (firstName != string.Empty && lastName != string.Empty)
            {
                return new User(firstName, lastName, age, PlaceOfWorkTextBox.Text);
            }
            else
            {
                throw new System.ApplicationException();
            }  
        }

        private void CleanTextBoxes()
        {
            FirstNameTextBox.Text = string.Empty;
            LastNameTextBox.Text = string.Empty;
            AgeextBox.Text = string.Empty;
            PlaceOfWorkTextBox.Text = string.Empty;
            PathTextBox.Text = string.Empty;
        }
    }
}
