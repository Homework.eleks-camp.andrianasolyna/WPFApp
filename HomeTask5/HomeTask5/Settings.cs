﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using HomeTask5.Interfaces;

namespace HomeTask5
{
    public class Settings: ISettings
    {
        private static Settings _instanse;
        public string FilePath
        {
            get;
            set;
        }
        private Settings(string filePath)
        {
            FilePath = filePath;
        }
        public static Settings GetInstanse(string filePath)
        {
            return _instanse ?? (_instanse = new Settings(filePath));
        }
    }
}
