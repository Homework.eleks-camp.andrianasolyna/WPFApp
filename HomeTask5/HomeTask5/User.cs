﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using HomeTask5.Annotations;
using HomeTask5.Interfaces;

namespace HomeTask5
{
    public class User: INotifyPropertyChanged
    {
        public string FirstName
        {
            get; set;
        }
        public string LastName { get; set; }
        public int Age { get; set; }
        public string PlaceOfWork { get; set; }

        public User()
        {
            FirstName = string.Empty;
            LastName = string.Empty;
            Age = 0;
            PlaceOfWork = string.Empty;
        }

        public User(string firstName, string lastName, int age, string placeOfWork)
        {
            FirstName = firstName;
            LastName = lastName;
            Age = age;
            PlaceOfWork = placeOfWork;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
