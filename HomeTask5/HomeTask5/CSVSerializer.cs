﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomeTask5.Interfaces;

namespace HomeTask5
{
    public class CSVSerializer: ISerializer
    {
        public string Serialize(User user)
        {
            return $"{user.FirstName}, {user.LastName}, {user.Age}, {user.PlaceOfWork}";
        }
    }
}
